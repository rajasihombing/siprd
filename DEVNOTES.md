# SIPRD Development Notes
## What is this document for?
Feel free to note down any things the rest of the team should know about this app. This includes short notes on what has to be changed, how to run it, etc.

## Notes
- If `python manage.py makemigrations` fails, try `python manage.py makemigrations main` first, then migrate as usual.

---
## Developing App Locally ⚙
Follow these steps if you want to run, develop or make changes on your local environment

1. Clone this repository

```bash
git clone git@gitlab.cs.ui.ac.id:ppl-fasilkom-ui/2022/kelas-c/peer-review-kepangkatan-akademik-pepe-el-vande/peer-review-kepangkatan-akademik-siprd.git
```

2. Navigate to the directory `peer-review-kepangkatan-akademik-siprd`

### Database 
1. Run docker-compose to build all images and run `db` services only

```bash
docker-compose up -d db
```
### Backend

1. Navigate to the directory `siprd`

2. Create a python virtual environment

```bash
python -m venv env # depending on your computer/os, it may be python3
```

3. Activate the virtual environment

```bash
source env/bin/activate # MacOS / Linux

env\Scripts\activate # Windows
```

4. Install the required dependencies

```bash
pip install -r requirements.txt
```

5. Run make migrations

```bash
python manage.py makemigrations
python manage.py migrate
```

5. Run backend server

```bash
python manage.py runserver
```

### Frontend

1. Navigate to the directory `siprd-fe`

2. Install the dependencies
```bash
npm install
```

3. Run the server
```bash
npm run serve
```
### Testing App

1. Navigate to the directory `siprd`

2. Run backend tests

```bash
coverage run --include="./*" --omit="venv/*,manage.py,siprd/*,env/*" manage.py test
```

3. Showing coverage reports

```bash
coverage report -m
```
## Production 📦
Currently our team deploys manually by uploading frontend & backend images to docker hub. Then pull and run it on our server hosting.

1. Login docker to siprd account

```bash
docker login
```

2. Navigate to the directory `siprd`

3. Build and push docker images

```bash
docker build -t siprd/siprd-be:1.0 .
docker push siprd/siprd-be:1.0
```

4. Navigate to the directory `siprd-fe`

5. Build and push docker images

```bash
docker build -t siprd/siprd-fe:1.0  .
docker push siprd/siprd-fe:1.0
```

After uploading images to Docker Hub, go to your Server Hosting console (Google Compute Engine) and install `docker`, `docker-compose` and `rsync` (see reference links) and open port 8000. Maybe you need to install `Node` (v16) or `npm` too.

1. Copy `production.yml` to server console

2. Run `docker-compose` from `production.yml` 

```bash
docker-compose -f production.yml up --build -d
```

⚠ Don't forget to replace IP, ClientID, Client Secret and others to fit your configuration

---
References:
- Install docker : https://docs.docker.com/engine/install/
- Add user to docker group : https://docs.docker.com/engine/install/linux-postinstall/
- Install docker-compose : https://docs.docker.com/compose/install/
- Install rsync : https://nixcp.com/bash-rsync-command-not-found/
- Install Node and npm : https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
- Login as root gcp : https://stackoverflow.com/questions/39572841/how-to-login-as-root-in-google-cloud-compute-engine
- docker hub : https://hub.docker.com/
- docker-compose up : https://stackoverflow.com/questions/39988844/docker-compose-up-vs-docker-compose-up-build-vs-docker-compose-build-no-cach
- docker command : https://towardsdatascience.com/12-essential-docker-commands-you-should-know-c2d5a7751bb5
- google auth : https://www.youtube.com/watch?v=d7OxfJZOIhQ
- google auth git example : https://github.com/CryceTruly/incomeexpensesapi
- google auth py : https://github.com/googleapis/google-api-python-client
