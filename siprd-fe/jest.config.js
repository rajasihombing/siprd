const config = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  verbose: false,
  transform: {
    '^.+\\.ts?$': 'ts-jest',
    '^.+\\.(js|jsx)$': 'babel-jest',
  },
  transformIgnorePatterns: [
    '<rootDir>/node_modules/(?!vue-axios/)',
  ],

};

module.exports = config;
