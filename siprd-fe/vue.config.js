module.exports = {
  transpileDependencies: ['vuetify'],
  publicPath: '',
  lintOnSave: process.env.NODE_ENV !== 'production',
};
