import { shallowMount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vue from 'vue';
import VueAxios from 'vue-axios';
import axios from 'axios';
import Successs from '@/views/Success.vue';

describe('Success.vue', () => {
  it('renders Success.vue when authenticated', () => {
    const myvue = Vue.use(VueAxios, axios);
    const routes = [
      {
        path: '/',
      },
    ];

    const router = new VueRouter({ routes });
    const msg = 'Welcome, ';
    const wrapper = shallowMount(Successs, {
      propsData: { msg },
      myvue,
      router,
    });
    expect(wrapper.text()).toContain(msg);
  });
});
