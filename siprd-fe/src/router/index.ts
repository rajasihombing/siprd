import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Ping from '../views/Ping.vue';
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import AddAccount from '../views/AddAccount.vue';
import EditAccount from '../views/EditAccount.vue';
import Success from '../views/Success.vue';
import Dashboard from '../views/Dashboard.vue';
import RegisterSuccess from '../views/RegisterSuccess.vue';
import AccountList from '../views/AccountList.vue';
import AddKaril from '../views/AddKaril.vue';
import EditKaril from '../views/EditKaril.vue';
import AssignReviewer from '../views/AssignReviewer.vue';
import KarilList from '../views/KarilList.vue';
import ViewKaril from '../views/ViewKaril.vue';
import TokenError from '../views/TokenError.vue';
import ResetPassword from '../views/ResetPassword.vue';
import ForgetPasswordRequest from '../views/ForgetPasswordRequest.vue';
import ChangePassword from '../views/ChangePassword.vue';
import CreateReview from '../views/CreateReview.vue';
import ViewReview from '../views/ViewReview.vue';
import Panduan from '../views/Panduan.vue';


Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Landing',
    redirect: () => {
      if (localStorage.access) {
        return '/dashboard';
      }
      return '/login';
    },
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/ping',
    name: 'Ping',
    component: Ping,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
  },
  {
    path: '/add-account',
    name: 'AddAccount',
    component: AddAccount,
  },
  {
    path: '/edit-account',
    name: 'EditAccount',
    component: EditAccount,
  },
  {
    path: '/your-account',
    name: 'Success',
    component: Success,
  },
  {
    path: '/welcome',
    name: 'Welcome',
    component: RegisterSuccess,
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
  },
  {
    path: '/account-list',
    name: 'AccountList',
    component: AccountList,
  },
  {
    path: '/karil-list',
    name: 'KarilList',
    component: KarilList,
  },
  {
    path: '/view-karil',
    name: 'ViewKaril',
    component: ViewKaril,
  },
  {
    path: '/add-karil',
    name: 'AddKaril',
    component: AddKaril,
  },
  {
    path: '/edit-karil',
    name: 'EditKaril',
    component: EditKaril,
  },
  {
    path: '/assign-reviewer',
    name: 'AssignReviewer',
    component: AssignReviewer,
  },
  {
    path: '/token-error',
    name: 'TokenError',
    component: TokenError,
  },
  {
    path: '/reset-password/:token/:username/:uidb',
    name: 'ResetPassword',
    component: ResetPassword,
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    component: ForgetPasswordRequest,
  },
  {
    path: '/change-password',
    name: 'ChangePassword',
    component: ChangePassword,
  },
  {
    path: '/create-review',
    name: 'CreateReview',
    component: CreateReview,
  },
  {
    path: '/detail-review',
    name: 'DetailReview',
    component: ViewReview,
  },
  {
    path: '/panduan',
    name: 'Panduan',
    component: Panduan,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

async function isAuthenticated(): Promise<boolean> {
  let authenticated = false;
  if (localStorage.access) {
    let status;
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.access}`,
      },
    };
    await Vue.axios
      .get(
        `${process.env.VUE_APP_BACKEND_URL || ""}/api/auth/ping-with-auth`,
        config
      )
      .then((res: { status: number }) => {
        if (res.status === 200) {
          authenticated = true;
        }
      })
      .catch((err) => {
        status = err.response.status;
      });
    if (status === 401) {
      if (localStorage.refresh) {
        const payload = {
          refresh: localStorage.refresh,
        };
        await Vue.axios
          .post(
            `${process.env.VUE_APP_BACKEND_URL || ""}/api/auth/token/refresh/`,
            payload
          )
          .then((res) => {
            if (res.status === 200) {
              localStorage.setItem("refresh", res.data.refresh);
              localStorage.setItem("access", res.data.access);
              authenticated = true;
            }
          })
          .catch((err) => {
            localStorage.removeItem("refresh");
            localStorage.removeItem("access");
          });
      } else {
        localStorage.removeItem("access");
      }
    }
  }
  return authenticated;
}

// No need to check authentication when redirecting from here
const authExceptions = [
  '/login', '/Register', '/welcome', '/forgot-password', '/reset-password/'
];
router.beforeEach(async (to, from, next) => {
  // console.log(`Going from ${from.path}`);
  // console.log(`Going to ${to.name}`);
  if (to.path !== null && to.path !== undefined && authExceptions.some(path => to.path.includes(path)) ) {
    next();
  } else if (to.name !== 'Login') {
    const authenticated = await isAuthenticated();
    if (authenticated) {
      next();
    } else {
      next('/login');
    }
  } else next();
});

export default router;
