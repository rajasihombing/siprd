from django.test import TestCase
from django.test import TestCase
from main.models import User, KaryaIlmiah
from rest_framework.test import APIClient

# import re
# from urllib import request, response


# NOTE: These tests suck, feel free to refactor.
class SIPRDUnitTest(TestCase):
    login_url = "/api/auth/token/"
    manage_karil = "/api/karil/manage-karil/"
    karil_summary = "/api/karil/get-karil-summary/"
    karil_form = "/api/karil/get-karil-form/"
    manage_users_url = "/api/manage-users/"
    export_karil = "/api/karil/export-list-karil/"
    header_prefix = "Bearer "
    email = "test.user@example.com"
    full_name = "Test User"
    username = "awe"
    password = "awe"


    def setUp(self):
        self.client = APIClient()

        self.tester = User.objects.create_user(
            username = self.username,
            email = self.email,
            password = self.password,
            full_name = self.full_name,
            university = 'UI',
            field_of_study = 'Art',
            position = 'Lektor',
            role = 'Admin'
        )

        self.admin = User.objects.create_user(
            username = 'admin',
            email = 'admin@email.com',
            password = 'admin',
            full_name = 'admin satu',
            university = 'UI',
            field_of_study = 'ML',
            position = 'Lektor Kepala',
            role = 'Reviewer'
        )

        self.dosen = User.objects.create_user(
            username = "dosen1",
            email = 'dosen1@email.com',
            password = "dosen1",
            full_name = 'dosen',
            university = 'UI',
            field_of_study = 'Art',
            position = 'Lektor',
            role = 'Dosen'
        )
    
        self.admin.save()
        self.dosen.save()
        self.tester.save()
        
    def login(self):
        response = self.client.post(
            self.login_url,
            {
                'username': self.username,
                'password': self.password
            }, format='json')

        _, access = response.json().values()
        return access

    def loginAsDosen(self):
        response = self.client.post(
            self.login_url,
            {
                'username': 'dosen1',
                'password': 'dosen1'
            }, format='json')

        _, access = response.json().values()
        return access

    def loginAsAdmin(self):
        response = self.client.post(
            self.login_url,
            {
                'username': 'admin',
                'password': 'admin'
            }, format='json')

        _, access = response.json().values()
        return access
    
    def test_export_karil(self):
        access = self.loginAsAdmin()
        self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
        response = self.client.get(
            self.export_karil
        )
        self.assertEqual(response.status_code, 200)
    # def test_karil_is_created(self):
    #     self.tester = User.objects.create_user(
    #         username = self.username,
    #         email = self.email,
    #         password = self.password,
    #         full_name = self.full_name,
    #         university = 'UI',
    #         field_of_study = 'Art',
    #         position = 'Lektor',
    #         role = 'Admin'
    #     )
        
    #     access = self.login()
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)

    #     response = self.client.post(
    #         self.manage_karil,
    #         {
    #         'pemilik' : 'awe',
    #         'judul' : 'aaa111',
    #         'category' : 'aaa111',
    #         'status' : 'Not Reviewed Yet',
    #         'promotion' : 'Guru Besar/Professor'
    #         })

    #     print(response)
        
        # self.assertEqual(response.status_code, 201)


    # def test_karil_is_created_api(self):
    #     access = self.login()
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
        

        # response = self.client.post(
        #     self.manage_karil,
        #     {
        #     'pemilik' : self.tester,
        #     'judul' : 'aaa111',
        #     'category' : 'aaa111',
        #     'status' : 'Not Reviewed Yet',
        #     'promotion' : 'Guru Besar/Professor',})
        # self.assertEqual(response.status_code, 201)      

    # def test_karil_is_deleted(self):
    #     access = self.login()
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)

        # response = self.client.delete(
        #     self.manage_karil,
        #     {
        #         'username': 'doesnotexist'
        #     }
        # )
        # karya_ilmiah = KaryaIlmiah.objects.all()
        # self.assertEqual(karya_ilmiah.count(),1)
        # karya_ilmiah[0].delete()
        # self.assertEqual(karya_ilmiah.count(),0)