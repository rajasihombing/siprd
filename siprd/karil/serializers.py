from dataclasses import fields
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed

from main.models import KaryaIlmiah
from django.contrib.auth.tokens import PasswordResetTokenGenerator

class KaryaIlmiahSerializer(serializers.ModelSerializer):

    class Meta:
        model = KaryaIlmiah
        fields = [
            'karil_id',
            'pemilik',
            'judul',
            'journal_data',
            'link_origin',
            'link_repo',
            'link_indexer',
            'link_simcheck',
            'link_correspondence',
            'indexer',
            'category',
            'status',
            'promotion',
            'reviewers',
            'reviews'
        ]
