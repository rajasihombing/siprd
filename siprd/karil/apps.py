from django.apps import AppConfig


class KarilConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'karil'
