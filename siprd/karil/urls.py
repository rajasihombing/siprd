from django.urls import path, include, re_path
from . import views


app_name = "karil"

urlpatterns = [
  # path("ping", views.ping, name="ping"),
  path("get-karil-summary/", views.GetLinkedKarils.as_view()),
  path("manage-karil/", views.ManageKaril.as_view()),
  path("get-karil-form/", views.GetSpecificKarilForm.as_view()),
  path("export-list-karil/", views.download_xlss)
]
