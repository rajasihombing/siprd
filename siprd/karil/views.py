# from main.serializers import KaryaIlmiahSerializer
from xmlrpc.client import Boolean
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from main.models import KaryaIlmiah, User, Review
from rest_framework import status
from main.serializers import UserSerializer, KaryaIlmiahSerializer

from openpyxl import load_workbook, Workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import Border, Side, Alignment, PatternFill
import copy
from django.http import HttpResponse


import logging

logger = logging.getLogger(__name__)

# Create your views here.

# Get user data
# For use with getting user roles for authorization and others.


def get_user_data(request):
    username = request.user.username
    user = User.objects.filter(username=username).first()
    serializer = UserSerializer(user)
    return serializer.data


class Register(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            account = serializer.save()
            if account:
                return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetLinkedKarils(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        logger.info("Checking for linked karils...")
        requested_username = request.user.username

        karils = KaryaIlmiahSerializer(KaryaIlmiah.objects.filter(
            pemilik=requested_username), many=True)

        if len(karils.data) != 0:
            return Response(karils.data, status=status.HTTP_200_OK)
        else:
            # No matching karils
            return Response(status=status.HTTP_204_NO_CONTENT)


class ManageKaril(APIView):
    permission_classes = [IsAuthenticated]
    forbidden_role_msg = {
        'message': 'You are not authorized to modify this review form.'}
    serializer_class = KaryaIlmiahSerializer

    # Passes request data to serializer
    # Works just like register API
    # Creates Stage 1 review form
    def post(self, request):
        user_data = get_user_data(request)
        user_role = user_data['role']

        # Dosen are not allowed to create review forms!
        if (user_role == "Reviewer" or user_role == "Dosen"):
            return Response(self.forbidden_role_msg, status=status.HTTP_401_UNAUTHORIZED)

        data = request.data
        print(data)
        try:
            assert(data['pemilik'] == User.objects.filter(
                username=data['pemilik']).first().username, True)
        except User.DoesNotExist:
            return Response({'This author does not exist!'}, status=status.HTTP_404_NOT_FOUND)

        serializer = KaryaIlmiahSerializer(data=request.data)
        if serializer.is_valid():
            review = serializer.save()
            if review:
                return Response({request.data['judul'] + ' was queued for review succesfully!'}, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # Displays ALL submitted karils
    # Used for debugging
    # Can be deleted if unneeded
    def get(self, request):
        karil_list = KaryaIlmiah.objects.all()
        serializer = KaryaIlmiahSerializer(karil_list, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    # Updates Stage 1 review form into stage 2
    def put(self, request):
        """
        Stage 2 Review Form
        Admin and SDMPT can edit the review form and assign reviewers

        :param request.data['review']: existing stage 1 review form
        :return: updated stage 1 -> stage 2 review form
        """

        user_data = get_user_data(request)
        user_role = user_data['role']

        if (user_role == "Admin" or user_role == "SDM PT"):
            karil = None
            try:
                karil = KaryaIlmiah.objects.get(
                    karil_id=request.data['karil_id'])
            except KaryaIlmiah.DoesNotExist:
                return Response({'message': 'This review form does not exist!'}, status=status.HTTP_404_NOT_FOUND)

            serializer = KaryaIlmiahSerializer(
                karil, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(self.forbidden_role_msg, status=status.HTTP_401_UNAUTHORIZED)

    # Deletes karil with a requested karil_id
    # Needs karil data that wants to be deleted in the request body
    def delete(self, request):
        user_data = get_user_data(request)
        user_role = user_data['role']

        # Checks if a dosen is trying to delete their own karil
        if (user_data['username'] == request.data['pemilik'] and user_role == "Dosen"):
            try:
                karil = KaryaIlmiah.objects.get(
                    karil_id=request.data['karil_id'])
            except KaryaIlmiah.DoesNotExist:
                return Response({'message': 'The paper you are trying to delete does not exist'}, status=status.HTTP_404_NOT_FOUND)
            karil.delete()
            return Response({request.data['judul'] + ' was deleted successfully!'}, status=status.HTTP_200_OK)
        else:
            return Response(self.forbidden_warning, status=status.HTTP_401_UNAUTHORIZED)


class GetSpecificKarilForm(APIView):
    def post(self, request):
        try:
            karil_list = KaryaIlmiah.objects.filter(
                karil_id=request.data['karil_id']).first()
            serializer = KaryaIlmiahSerializer(karil_list)
        except KaryaIlmiah.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(serializer.data, status=status.HTTP_200_OK)


def get_user_data(request):
    username = request.user.username
    user = User.objects.filter(username=username).first()
    serializer = UserSerializer(user)
    return serializer.data


def export_daftar_karil_to_xlss(user_data):
    """
        Export data karil
        Admin and dosen can export data karil to xlss file

        :param user data
        :return: workbook xlss file
    """
    wb = Workbook()
    ws = wb.active
    ws.title = "Daftar Karil"
    title_list = ['No.', 'Nama Dosen', 'Nama Penulis', 'Judul Karya Ilmiah',
                  'Data Jurnal', 'Repository', 'Indexer', 'Check Similarity', 'Reviewer', 'Reviewed']
    ws.append(title_list)

    list_karil = KaryaIlmiah.objects.all().order_by('karil_id') if user_data['role'] == 'Admin' or user_data['role'] == 'SDM PT' else KaryaIlmiah.objects.filter(
        pemilik__username=user_data["username"]).order_by('karil_id')
    num = 1
    for karil in list_karil:
        list_reviewer = [
            review.reviewer.username for review in Review.objects.filter(karil_id=karil)]
        value_row = []
        value_row.append(num)
        value_row.append(karil.pemilik.username)
        value_row.append(karil.pemilik.full_name)
        value_row.append(karil.judul)
        value_row.append(karil.journal_data)
        value_row.append(karil.link_repo)
        value_row.append(karil.link_indexer)
        value_row.append(karil.link_simcheck)
        value_row.append(",".join(list_reviewer))
        value_row.append(karil.status)
        ws.append(value_row)
        num = num + 1

    # Excel Styling #
    ws.column_dimensions['A'].width = 6
    for col in range(2, 11):
        ws.column_dimensions[get_column_letter(col)].width = 25
        if col == 6:
            ws.column_dimensions[get_column_letter(col)].width = 50

    # Row Size
    ws.row_dimensions[1].height = 40

    for row in range(2, len(list_karil) + 2):
        ws.row_dimensions[row].height = 60

    # Coloring & format
    for row in range(1, len(list_karil) + 2):
        for col in range(1, 11):
            ws["{}{}".format(get_column_letter(col), row)].alignment = Alignment(
                horizontal="center", vertical="center")

            ws[f'{get_column_letter(col)}{row}'].border = Border(left=Side(border_style="thin",
                                                                           color='00000000'),
                                                                 right=Side(border_style="thin",
                                                                            color='00000000'),
                                                                 top=Side(border_style="thin",
                                                                          color='00000000'),
                                                                 bottom=Side(border_style="thin",
                                                                             color='00000000'),
                                                                 )
            if(row == 1):
                ws[f'{get_column_letter(col)}{row}'].fill = PatternFill(
                    "solid", fgColor="F8CE65")

    for row in ws.iter_rows():
        for cell in row:
            alignment = copy.copy(cell.alignment)
            alignment.wrapText = True
            cell.alignment = alignment
    return wb


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def download_xlss(request):
    user_data = get_user_data(request)
    workbook = export_daftar_karil_to_xlss(user_data)
    response = HttpResponse(
        save_virtual_workbook(
            workbook=workbook
        ),
        content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = f'attachment; filename="Export_Karil-{user_data["username"]}.xlsx"'
    return response
