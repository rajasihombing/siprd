from dataclasses import fields
from rest_framework import serializers

from main.models import Review


class ReviewKarilSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = [
            "karil_id",
            "reviewer",
            "plagiarism_percentage",
            "linearity",
            "score_1",
            "score_2",
            "score_3",
            "score_4",
            "comment_1",
            "comment_2",
            "comment_3",
            "comment_4",
            "proposer_choice",
            "score_proposer"
        ]

class GetReviewKarilSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'

