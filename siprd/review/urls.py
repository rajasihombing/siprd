from django.urls import path, include, re_path
from . import views

app_name = "review"

urlpatterns = [
  # path("ping", views.ping, name="ping"),
  # path("manage-reviews/", views.ManageReviews.as_view()),
  path("manage-reviewers/", views.ManageReviewers.as_view()),
  path("get-all-dosen/", views.get_all_dosen),
  path('review-karil/', views.ReviewKaril.as_view(), name="review-karil"),
  path("export-review/", views.donwload_review_to_pdf)
]
