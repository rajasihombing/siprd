# from tkinter import CENTER
from django.shortcuts import get_object_or_404, render
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import ParseError
from yarg import get
from rest_framework.decorators import api_view, permission_classes

from main.models import Review, User, KaryaIlmiah
from main.serializers import UserSerializer
from .serializers import ReviewKarilSerializer, GetReviewKarilSerializer

from openpyxl import load_workbook, Workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import Border, Side, Alignment, PatternFill
import copy
from django.http import HttpResponse

import datetime
import locale

# locale.setlocale(locale.LC_TIME, "id_ID") 

# Create your views here.


class ReviewKaril(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user_data = get_user_data(request)
        list_review = Review.objects.all() if user_data['role'] == "Admin" or user_data['role'] == "SDM PT" else Review.objects.filter(
            reviewer__username=user_data["username"])

        if "review_id" in request.query_params:
            review = get_object_or_404(Review.objects.all(
            ), review_id=request.query_params["review_id"])
            serializer = GetReviewKarilSerializer(instance=review)
            return Response(serializer.data)

        if (("karil_id" in request.query_params) and ("?reviewer" in request.query_params)):
            review = get_object_or_404(Review.objects.all(
            ), karil_id__karil_id=request.query_params["karil_id"], reviewer__username=request.query_params["?reviewer"])
            serializer = GetReviewKarilSerializer(instance=review)
            return Response(serializer.data)

        # print(request.query_params)
        serializer = GetReviewKarilSerializer(instance=list_review, many=True)
        return Response(serializer.data)

    def put(self, request):
        reviewer = get_object_or_404(
            User.objects.all(), username=request.data["reviewer"])
        karil = get_object_or_404(
            KaryaIlmiah.objects.all(), karil_id=request.data["karil_id"])
        instance = get_object_or_404(
            Review.objects.all(), reviewer=reviewer, karil_id=karil)
        serializer = ReviewKarilSerializer(
            instance=instance, data=request.data)
        if serializer.is_valid():
            serializer.save()
            review = Review.objects.get(
                karil_id=instance.karil_id, reviewer=reviewer)
            review.status = "Submited"
            review.save()

            if karil.status == "Not Reviewed":
                karil.status = "In Review"

            list_review = Review.objects.all().filter(karil_id=karil)
            if karil.status == "In Review":
                flag = True
                for review_karil in list_review:
                    if review_karil.status == "Not Submited Yet":
                        flag = False
                        break
                if flag:
                    karil.status = "Reviewed"
            karil.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        raise ParseError(
            {
                "message": "Review request body is not valid",
                "data": serializer.data
            }
        )


def get_user_data(request):
    username = request.user.username
    user = User.objects.filter(username=username).first()
    serializer = UserSerializer(user)
    return serializer.data

class ManageReviewers(APIView):
    permission_classes = [IsAuthenticated]
    forbidden_role_msg = {'message': 'You must be an Admin or SDM PT to perform this action.'}
    position_exclusions = {
        'Asisten Ahli': [],
        'Lektor': ['Asisten Ahli'],
        'Lektor Kepala': ['Asisten Ahli', 'Lektor'],
        'Guru Besar/Professor': ['Asisten Ahli', 'Lektor', 'Lektor Kepala']
    }

    # Fetches all reviewers
    # Reviewers must be positioned equal to or higher than selected promotion rank.
    # e.g. dosen wants to be promoted to Lektor, reviewer cannot be Asisten Ahli.
    def get(self, request):
        user_data = get_user_data(request)
        user_role = user_data['role']

        if ( user_role == "Admin" or user_role == "SDM PT" ):
            # Selected promotion rank
            selected_role = request.data['position']
            user_list = (
                User.objects
                .filter(role='Dosen')
                .exclude(position__in=self.position_exclusions[selected_role])
                .order_by("date_joined")
                .reverse()
            )
            serializer = UserSerializer(user_list, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else: return Response(self.forbidden_role_msg, status=status.HTTP_401_UNAUTHORIZED)
    
    def post(self, request):
        user_data = get_user_data(request)
        user_role = user_data['role']
        karil_id = request.data['karil_id']
        review = None
        serializer = None
        karil = KaryaIlmiah.objects
        
        if ( user_role == "Admin" or user_role == "SDM PT" ):
            reviewers = request.data['reviewers']
            
            for reviewer in reviewers:
                aData = {
                    "karil_id" : karil_id,
                    "reviewer" : reviewer["username"],
                }
                this_karil = karil.get(karil_id = karil_id)
                serializer = ReviewKarilSerializer(data = aData)
                if serializer.is_valid():
                    review = serializer.save()
                    this_karil.reviews.add(review.review_id)

            if review:
                return Response({'a review was queued for review succesfully!'}, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                # else: return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_all_dosen(request):
    dosen = User.objects.filter(role='Dosen')
    serializer = UserSerializer(instance=dosen, many=True)
    return Response(serializer.data)


def export_review_to_pdf(karil, review):
    
    # o = win32com.client.Dispatch("Excel.Application")
    # o.Visible = False

    """
        Export data review

        :param user data
        :return: workbook xlss file
    """

    wb = Workbook()
    ws = wb.active
    ws.title = "Review"

    thin_border = Border(left=Side(style='thin'), 
                     right=Side(style='thin'), 
                     top=Side(style='thin'), 
                     bottom=Side(style='thin'))

    # new_wb = o.Workbooks.Open()

    value_row = ['HASIL PENILAIAN SEJAWAT SEBIDANG ATAU PEER REVIEW']
    ws.append(value_row)
    ws.merge_cells('A1:G1')

    value_row = ['A','Identitas karya ilmiah']
    ws.append(value_row)
    ws.merge_cells('B2:G2')

    value_row = ['1', 'Judul']
    value_row.append(karil['judul'])
    ws.append(value_row)

    value_row = ['2', 'Data Jurnal']
    value_row.append(karil['journal_data'])
    ws.append(value_row)

    value_row = ['3.1', 'Nama Penulis']
    value_row.append(karil['pemilik'])
    ws.append(value_row)


    value_row = ['3.2', 'link asli jurnal']
    value_row.append(karil['link_origin'])
    ws.append(value_row)

    value_row = ['3.3', 'link respository  jurnal']
    value_row.append(karil['link_repo'])
    ws.append(value_row)

    value_row = ['3.4', 'link  indexer']
    value_row.append(karil['link_indexer'])
    ws.append(value_row)

    value_row = ['3.5', 'link check similarity']
    value_row.append(karil['link_simcheck'])
    ws.append(value_row)

    value_row = ['3.6', 'link bukti korespondensi']
    value_row.append(karil['link_correspondence'])
    ws.append(value_row)

    peng_index = 'Peng-index : '+ str(karil['indexer'])
    value_row = ['B', peng_index]
    ws.append(value_row)
    ws.append([])

    value_row = ['C', 
    'Kategori karya ilmiah, dan nilai maksimal (pilih salah satu dengan memberikan tanda P)',
    '','','','','AK']
    ws.append(value_row)

    value_row = ['Buku']
    ws.append(value_row)

    value_row = ['1', 'Buku referensi','','','','','40']
    ws.append(value_row)

    value_row = ['2', 'Buku monograph','','','','','20']
    ws.append(value_row)

    value_row = ['3', 'Book chapter (internasional)','','','','', '15']
    ws.append(value_row)

    value_row = ['4', 'Book chapter (nasional)','','','','', '10']
    ws.append(value_row)

    ###

    value_row = ['Jurnal']
    ws.append(value_row)

    value_row = ['5', 'Jurnal internasional bereputasi (terindeks pada database internasional bereputasi dan berfaktor dampak)','','','','', '40']
    ws.append(value_row)

    value_row = ['6', 'Jurnal internasional terindeks pada basis data internasional bereputasi','','','','','30']
    ws.append(value_row)

    value_row = ['7', 'Jurnal internasional terindeks pada basis data non bereputasi','','','','','20']
    ws.append(value_row)

    value_row = ['8', 'Jurnal nasional terakreditasi Kemenristek Dikti','','','','','25']
    ws.append(value_row)

    value_row = ['9', 'Jurnal nasional terakreditasi Kemenristek Dikti peringkat 1 dan 2','','','','','25']
    ws.append(value_row)

    value_row = ['10', 'Jurnal nasional berbahasa Inggris atau bahasa resmi (PBB) terindeks pada basis data yang diakui Kemenristekdikti, contoh: CABI atau Index Copernicus International (ICI)','','','','','20']
    ws.append(value_row)

    value_row = ['11', 'Jurnal nasional berbahasa Indonesia terindeks pada basis data yang diakui Kemenristekdikti, contoh : akreditasi peringkat 5 dan 6','','','','','15']
    ws.append(value_row)
    
    value_row = ['12', 'Jurnal nasional','','','','','10']
    ws.append(value_row)

    value_row = ['13', 'Jurnal ilmiah yang ditulis dalam Bahasa Resmi PBB namun tidak memenuhi syarat syarat sebagai jurnal ilmiah internasional','','','','','10']
    value_row.append('')
    ws.append(value_row)

    ###

    value_row = ['Dipresentasikan secara oral dan dimuat dalam prosiding yang dipublikasikan (ber ISSN/ISBN)']
    ws.append(value_row)

    value_row = ['14', 'Internasional terindeks pada Scimagojr dan Scopus','','','','','30']
    ws.append(value_row)

    value_row = ['15', 'Internasional terindeks pada Scopus/IEEE Explore/SPIE','','','','','25']
    ws.append(value_row)

    value_row = ['16', 'Internasional','','','','','15']
    ws.append(value_row)

    value_row = ['17', 'Nasional','','','','','10']
    ws.append(value_row)  

    ###

    value_row = ['Disajikan dalam bentuk poster dan dimuat dalam prosiding yang dipublikasikan']
    ws.append(value_row)

    value_row = ['18', 'Internasional','','','','','10']
    ws.append(value_row)

    value_row = ['19', 'Nasional','','','','','5']
    ws.append(value_row)

    value_row = ['','Disajikan dalam seminar/simposium/lokakarya, tetapi tidak dimuat dalam prosiding yang dipublikasikan']
    ws.append(value_row)

    value_row = ['20', 'Internasional (bukti sertifikat)','','','','','3']
    ws.append(value_row)

    value_row = ['21', 'Nasional (bukti sertifikat)','','','','','10']
    ws.append(value_row)

    value_row = ['Hasil penelitian/pemikiran yang tidak disajikan dalam seminar/simposium/lokakarya']
    ws.append(value_row)

    value_row = ['22', 'Internasional','','','','','10']
    ws.append(value_row)

    value_row = ['23', 'Nasional','','','','','5']
    value_row.append('')
    ws.append(value_row)

    value_row = ['24', 'Hasil penelitian/pemikiran yang disajikan dalam koran/majalah populer/umum','','','','','1']
    value_row.append('')
    ws.append(value_row)

    value_row = ['25', 'Hasil penelitian atau pemikiran atau kerjasama industri yang tidak dipublikasikan','','','','','2']
    ws.append(value_row)
    
    value_row = ['26', 'Menerjemahkan/menyadur buku ilmiah, Diterbitkan dan diedarkan secara nasional','','','','','15']
    ws.append(value_row)

    value_row = ['27', 'Mengedit/menyunting karya ilmiah, Diterbitkan dan diedarkan secara nasional','','','','','10']
    ws.append(value_row)

    ###

    value_row = ['HKI - Membuat rancangan dan karya teknologi yang dipatenkan atau seni yang terdaftar di HAKI secara nasional atau internasional']
    ws.append(value_row)

    value_row = ['28', 'Internasional (paling sedikit diakui oleh 4 negara)','','','','','60']
    ws.append(value_row)

    value_row = ['29', 'Nasional','','','','','40']
    ws.append(value_row)

    value_row = ['30', 'Nasional, dalam bentuk paten sederhana yang telah memiliki sertifikat dari Direktorat Jenderal Kekayaan Intelektual, Kemenkumham','','','','','20']
    ws.append(value_row)

    value_row = ['31', 'Karya ciptaan desain industri, indikasi geografis yang telah memiliki sertifikat dari Direktorat Jenderal Kekayaan Intelektual, Kemenkumham (termasuk kategori ini: Buku/Modul Ajar)','','','','','15']
    ws.append(value_row)

    ###

    value_row = ['Membuat rancangan dan karya teknologi yang tidak dipatenkan; rancangan dan karya seni monumental yang tidak terdaftar di HAKI tetapi telah dipresentasikan pada forum yang teragenda']
    ws.append(value_row)

    value_row = ['32', 'Internasional','','','','','20']
    ws.append(value_row)

    value_row = ['33', 'Nasional','','','','','15']
    ws.append(value_row)

    value_row = ['34', 'Lokal','','','','','10']
    ws.append(value_row)

    value_row = ['35', 'Rancangan dan karya seni yang tidak terdaftar HAKI','','','','',]
    ws.append(value_row)

    ws.append([])
    ws.append([])

    value_row = ['Hasil penilaian validasi']
    ws.append(value_row)
    ws.merge_cells('A59:G59')

    value_row = ['', 'Aspek','Uraian/komentar penilaian']
    ws.append(value_row)

    value_row = ['D', 'Indikasi plagiasi (lihat check similarity)','similarity index 12 %, primary source 2%, tidak ada indikasi plagiasi']
    ws.append(value_row)

    value_row = ['E', 'Linearitas','terkait dengan bidang penelitian calon']
    ws.append(value_row)

    value_row = ['Hasil penilaian peer review']
    ws.append(value_row)
    ws.append([])
    ws.merge_cells('A63:G64')

    value_row = ['','Komponen yang dinilai', 'Komentar/ulasan peer reviewer (wajib diisi dan hindari komentar'+
    'yang hanya memuat satu dua kata seperti: sangat dalam, cukup baik, sangat berkualitas)','', 'Nilai  Maks', 'Nilai akhir']
    ws.append(value_row)

    value_row = ['F','Kelengkapan dan kesesuaian unsur publikasi (10%)', review['comment_1'],'', '4', review['score_1']]
    ws.append(value_row)

    value_row = ['G','Ruang lingkup,  kedalaman pembahasan, keterbaruan (30%)',review['comment_2'] ,'', '12', review['score_2']]
    ws.append(value_row)

    value_row = ['H','Kecukupan dan kemutakhiran data/informasi dan metodologi (30%)', review['comment_3'], '','12', review['score_3']]
    ws.append(value_row)

    value_row = ['I','Kelengkapan unsur dan kualitas penerbit,  hasil dan manfaat(30%)',review['comment_4'] ,'', '12', review['score_4']]
    ws.append(value_row)

    value_row = ['J','Jumlah Total = (100%)','','','40', str(review['score_1']+review['score_2']+review['score_3']+review['score_4'])]
    ws.append(value_row)

    value_row = ['K','Nilai Pengusul (pilih salah satu kriteria dibawah ini):','','','','']
    ws.append(value_row)

    value_row = ['K1','Nilai pengusul (penulis mandiri sekaligus koresponden) = 100%','','','','']
    ws.append(value_row)

    value_row = ['K2','Nilai pengusul (penulis pertama sekaligus koresponden) =60%','','','','']
    ws.append(value_row)

    value_row = ['K3','Nilai pengusul (penulis pertama bukan koresponden atau penulis koresponden bukan penulis pertama, jumlah penulis 2) = 50%','','','','']
    ws.append(value_row)

    value_row = ['K4','Nilai pengusul (penulis pertama bukan koresponden atau penulis koresponden bukan penulis pertama, jumlah penulis > 2) =40%','','','','']
    ws.append(value_row)

    value_row = ['K5','Nilai pengusul (penulis pendamping, penulis pertama sekaligus koresponden) = 40%'+' dibagi jumlah penulis pendamping','','','','']
    ws.append(value_row)

    value_row = ['K6','Nilai pengusul (penulis pendamping, penulis pertama bukan koresponden) = 20%'+' dibagi jumlah penulis pendamping','','','','']
    ws.append(value_row)

    tanggal = str(datetime.date.today())
    d = datetime.datetime.strptime(tanggal, '%Y-%m-%d')
    d = d.strftime('%d %b %Y')

    ws['B79'] = 'Depok'
    ws['C79'] = str(d)

    ws['B80'] = 'Reviewer'

    ws['B84'] = 'Nama'
    ws['C84'] = ':  Prof. Heru Suhartanto, Drs., M.Sc., Ph.D.'


    ws['B85'] = 'NIP'
    ws['C85'] = ': 196104191992031001'

    ws['B86'] = 'Unit Kerja '
    ws['C86'] = ': Fakultas Ilmu Komputer, UI'


    ###
    # Merge Cell #

    for i in range(3,11):
        ws.merge_cells('C'+str(i)+':G'+str(i))

    ws.merge_cells('A11:A12')
    ws.merge_cells('B11:G12')

    beda = [14,19,29,34,40,47,52]
    for i in range(13,57):
        if(i in beda):
            ws.merge_cells('A'+str(i)+':G'+str(i))
        else:
            ws.merge_cells('B'+str(i)+':F'+str(i))

    ws.merge_cells('C60:G60')
    ws.merge_cells('C61:G61')
    ws.merge_cells('C62:G62')

    for i in range(65,71):
        if(i==70):
            ws.merge_cells('B'+str(i)+':D'+str(i))
        else:
            ws.merge_cells('C'+str(i)+':D'+str(i))
        ws.merge_cells('F'+str(i)+':G'+str(i))

    for i in range(72,78):
        ws.merge_cells('B'+str(i)+':E'+str(i))
        ws.merge_cells('F'+str(i)+':G'+str(i))

    ws.merge_cells('B71:G71')
    ws.merge_cells('C84:D84')
    ws.merge_cells('C85:D85')
    ws.merge_cells('C86:D86')

    ###

    # Excel Styling #
    ws.column_dimensions['A'].width = 4
    ws.column_dimensions['B'].width = 15
    ws.column_dimensions['C'].width = 17
    ws.column_dimensions['D'].width = 30
    ws.column_dimensions['E'].width = 11
    ws.column_dimensions['F'].width = 8
    ws.column_dimensions['F'].width = 14

    # Row Size
    ws.row_dimensions[1].height = 14

    # for row in range(2, len(list_karil) + 2):
    #     ws.row_dimensions[row].height = 60

    # Coloring & format

    for row in range(1, 78):
        for col in range(1, 8):
        
            if ((not(row in beda)) and (col == 1 or col == 7)):
                ws["{}{}".format(get_column_letter(col), row)].alignment = Alignment(
                horizontal="center", vertical="center")
            
            if(row < 11 ):
                ws[f'{get_column_letter(col)}{row}'].border = Border(left = Side(border_style="thin",
                                                                            color='00000000'),
                                                                 right=Side(border_style="thin",
                                                                            color='00000000'),
                                                                 top=Side(border_style="thin",
                                                                          color='00000000'),
                                                                 bottom=Side(border_style="thin",
                                                                             color='00000000'),
                                                                 )
            else:
                ws[f'{get_column_letter(col)}{row}'].border = Border(left = Side(border_style="thin",
                                                                            color='00000000'),
                                                                    right=Side(border_style="thin",
                                                                                color='00000000'),
                                                                    top=Side(border_style="thin",
                                                                            color='00000000'),
                                                                    bottom=Side(border_style="thin",
                                                                                color='00000000'),
                                                                    )
    
    for row in ws.iter_rows():
        for cell in row:
            alignment = copy.copy(cell.alignment)
            alignment.wrapText = True
            cell.alignment = alignment

    return wb

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def donwload_review_to_pdf(request):
    user_data = get_user_data(request)
    workbook = export_review_to_pdf(request.data['karilData'],request.data['reviewData'])
    response = HttpResponse(
        save_virtual_workbook(
            workbook=workbook
        ),
        content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = f'attachment; filename="Export_Karil-{user_data["username"]}.xlsx"'
    return response

###

# def openWorkbook(xlapp, xlfile):
#     try:        
#         xlwb = xlapp.Workbooks(xlfile)            
#     except Exception as e:
#         try:
#             xlwb = xlapp.Workbooks.Open(xlfile)
#         except Exception as e:
#             print(e)
#             xlwb = None                    
#     return(xlwb)
# print('awe1')
# try:
#     excel = win32.gencache.EnsureDispatch('Excel.Application')

#     WB_PATH = 'C:\\Users\\billy\\Documents\\Kuliah\\Semester6\\PPL\\RealPPL\\peer-review-kepangkatan-akademik-siprd\\siprd\\review\\export-karil.xlsx'
#     PATH_TO_PDF = 'C:\\Users\\billy\\Documents\\Kuliah\\Semester6\\PPL\\RealPPL\\peer-review-kepangkatan-akademik-siprd\\siprd\\review\\pdf-karil.pdf'

#     # wb = excel.Workbooks.Open(WB_PATH)
#     wb = openWorkbook(excel, WB_PATH)
    
#     ws_index_list  = [1]
#     ws = wb.Worksheets(ws_index_list).Select()

#     # excel.Visible = False
    
#     awe12 = wb.SaveAs(PATH_TO_PDF)
#     print('awek1')
#     print(awe12)
#     print('awek2')

# except com_error as e:
#     print('failed.')

# finally:
#     # RELEASES RESOURCES
#     ws = None
#     # wb = None
#     # excel = None

#     wb.Close()
#     excel.Quit()