from os import access
from django.test import TestCase
from rest_framework.test import APIClient
from main.models import User, KaryaIlmiah, Review
# Create your tests here.


class ReviewAppUnitTest(TestCase):
    login_url = "/api/auth/token/"
    review_karil_url = "/api/review/review-karil/"
    header_prefix = "Bearer "
    email = "test.user@example.com"
    full_name = "Test User"
    username = "tester"
    password = "test"

    def setUp(self):
        self.client = APIClient(HTTP_HOST='example.com')
        self.tester = User.objects.create_user(
            username=self.username,
            email=self.email,
            password=self.password,
            full_name=self.full_name,
            university='UI',
            field_of_study='Art',
            position='Lektor',
            role='Admin'
        )
        self.reviewer = User.objects.create_user(
            username='reviewer_satu',
            email='reviewer_satu@email.com',
            password='reviewer_satu',
            full_name='reviewer satu',
            university='UI',
            field_of_study='ML',
            position='Lektor Kepala',
            role='Reviewer'
        )
        self.dosen = User.objects.create_user(
            username="dosen1",
            email='dosen1@email.com',
            password="dosen1",
            full_name='dosen',
            university='UI',
            field_of_study='Art',
            position='Lektor',
            role='Dosen'
        )
        self.karil = KaryaIlmiah(karil_id=1, pemilik=self.dosen)
        self.review_karil = Review(
            review_id=1,
            karil_id=self.karil,
            reviewer=self.reviewer,
            plagiarism_percentage=0,
            score_1=0,
            score_2=0,
            score_3=0,
            score_4=0,
            comment_1="",
            comment_2="",
            comment_3="",
            comment_4="",
            score_proposer=0

        )
        self.karil.save()
        self.review_karil.save()

    def login(self):
        response = self.client.post(
            self.login_url,
            {
                'username': self.username,
                'password': self.password
            }, format='json')
        _, access = response.json().values()
        return access

    def test_review_karil(self):
        access = self.login()
        self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
        response = self.client.put(
            self.review_karil_url,
            {
                "karil_id": 1,
                "reviewer": "reviewer_satu",
                "plagiarism_percentage": 11,
                "linearity": "2",
                "score_1": "23",
                "score_2": "32",
                "score_3": "32",
                "score_4": "32",
                "comment_1": "aaaaas",
                "comment_2": "aaa",
                "comment_3": "aa",
                "comment_4": "aaaa",
                "score_proposer": 2
            }
        )
        self.assertEqual(response.status_code, 200)

    # def test_review_karil_invalid(self):
    #     access = self.login()
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    #     response = self.client.put(
    #         self.review_karil_url,
    #         {
    #             "karil_id": 1,
    #             "reviewer": "reviewer_satu",
    #             "plagiarism_percentage": "dddasdadasd",
    #             "linearity": "2",
    #             "score_1": "23",
    #             "score_2": "32",
    #             "score_3": "32",
    #             "score_4": "32",
    #             "comment_1": "aaaaas",
    #             "comment_2": "aaa",
    #             "comment_3": "aa",
    #             "comment_4": "aaaa",
    #             "score_proposer": 2
    #         }
    #     )
    #     self.assertEqual(response.status_code, 400)

    def test_review_karil_in_review(self):
        access = self.login()
        self.karil.status = "In Review"
        self.review_karil.status = "Not Submited Yet"
        self.karil.save()
        self.review_karil.save()
        self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
        response = self.client.put(
            self.review_karil_url,
            {
                "karil_id": 1,
                "reviewer": "reviewer_satu",
                "plagiarism_percentage": 11,
                "linearity": "2",
                "score_1": "23",
                "score_2": "32",
                "score_3": "32",
                "score_4": "32",
                "comment_1": "aaaaas",
                "comment_2": "aaa",
                "comment_3": "aa",
                "comment_4": "aaaa",
                "score_proposer": 2
            }
        )
        self.assertEqual(response.status_code, 200)


    def test_get_review_karil_as_dosen(self):
        access = self.login()
        self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
        response = self.client.get(
            self.review_karil_url
        )
        self.assertEqual(response.status_code, 200)
