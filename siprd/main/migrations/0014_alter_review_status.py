# Generated by Django 3.2.7 on 2022-05-22 14:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_merge_20220522_1956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='status',
            field=models.CharField(choices=[('Not Submited Yet', 'Not_Submited_Yet'), ('Submited', 'Submited')], default='Not Submited Yet', max_length=254),
        ),
    ]
