from django.contrib import admin
from .models import User,Review,KaryaIlmiah

# Register your models here.
admin.site.register(User)
admin.site.register(KaryaIlmiah)
admin.site.register(Review)