import re
from urllib import request, response
from django.test import TestCase
from .models import User, KaryaIlmiah, Review
from rest_framework.test import APIClient, force_authenticate, APIRequestFactory
from django.urls import resolve


# NOTE: These tests suck, feel free to refactor.
class SIPRDUnitTest(TestCase):

    update_url = "/api/update"
    register_url = "/api/auth/register"
    login_url = "/api/auth/token/"
    manage_users_url = "/api/user/manage-user/"
    get_all_reviewer_url = "/api/get-all-reviewer"
    is_user_exist_url =  "/api/user/is-user-exists"
    reset_password_complete = "/api/password-reset-complete"
    header_prefix = "Bearer "
    email = "test.user@example.com"
    full_name = "Test User"
    username = "tester"
    password = "test"

    def setUp(self):
        self.client = APIClient()

        self.tester = User.objects.create_user(
            username = self.username,
            email = self.email,
            password = self.password,
            full_name = self.full_name,
            university = 'UI',
            field_of_study = 'Art',
            position = 'Lektor',
            role = 'Admin'
        )

        self.reviewer = User.objects.create_user(
            username = 'reviewer_satu',
            email = 'reviewer_satu@email.com',
            password = 'reviewer_satu',
            full_name = 'reviewer satu',
            university = 'UI',
            field_of_study = 'ML',
            position = 'Lektor Kepala',
            role = 'Reviewer'
        )

    def login(self):
        response = self.client.post(
            self.login_url,
            {
                'username': self.username,
                'password': self.password
            }, format='json')

        _, access = response.json().values()
        return access

    # def loginAsDosen(self):
    #     response = self.client.put(
    #         self.manage_users_url,
    #         {
    #             'username': self.username,
    #             'email': self.email,
    #             'password': self.password,
    #             'full_name': self.full_name,
    #             'university': 'UGM',
    #             'expertise': 'Art',
    #             'position': 'Lektor',
    #             'role': 'Dosen'
    #         },
    #         format='json'
    #     )
    #     return response

    # def test_ping_url_exists(self):
    #     response = self.client.get('/api/auth/ping')
    #     self.assertEqual(response.status_code, 200)

    # def test_ping_can_return_JSON_data(self):
    #     response = self.client.get('/api/auth/ping')
    #     self.assertEqual(response.json().get('message'), 'Ping success!')


    # def test_api_register_new_user_returns_HTTP_Status_201_CREATED(self):
    #     response = self.client.post(
    #         self.register_url,
    #         {
    #             'username': 'test',
    #             'email': self.email,
    #             'password': 'test',
    #             'full_name': self.full_name,
    #             'university': 'UI',
    #             'expertise': 'Art',
    #             'position': 'Lektor',
    #             'role': 'Admin'
    #         },
    #         format='json')
    #     self.assertEqual(response.status_code, 201)

    # def test_api_register_new_user_incomplete_returns_HTTP_Status_400_BAD_REQUEST(self):
    #     response = self.client.post(
    #         self.register_url,
    #         {
    #             'username': 'test'
    #         })
    #     self.assertEqual(response.status_code, 400)

    # def test_login_new_user_returns_HTTP_Status_OK(self):
    #     self.client.post(
    #         self.register_url,
    #         {
    #             'username': 'test',
    #             'email': self.email,
    #             'password': 'test',
    #             'full_name': self.full_name,
    #             'university': 'UI',
    #             'expertise': 'Art',
    #             'position': 'Lektor',
    #             'role': 'Admin'
    #         },
    #         format='json')

    #     response = self.client.post(
    #         self.login_url,
    #         {
    #             'username': 'test',
    #             'password': 'test'
    #         }, format='json')

    #     self.assertEqual(response.status_code, 200)

    # def test_login_new_user_returns_JWT_token_pair(self):
    #     self.client.post(
    #         self.register_url,
    #         {
    #             'username': 'test',
    #             'email': self.email,
    #             'password': 'test',
    #             'full_name': self.full_name,
    #             'university': 'UI',
    #             'expertise': 'Art',
    #             'position': 'Lektor',
    #             'role': 'Admin'
    #         },
    #         format='json')

    #     response = self.client.post(
    #         self.login_url,
    #         {
    #             'username': 'test',
    #             'password': 'test'
    #         }, format='json')

    #     access, refresh = response.json().values()
    #     self.assertIsNotNone(access)
    #     self.assertIsNotNone(refresh)


    # ## == Edit Data Tests == ##
    # def test_edit_user_data_returns_HTTP_OK(self):
    #     access = self.login()
    #
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    #     response = self.client.put(
    #         self.manage_users_url,
    #         {
    #             'username': self.username,
    #             'email': self.email,
    #             'password': self.password,
    #             'full_name': self.full_name,
    #             'university': 'UGM',
    #             'expertise': 'Art',
    #             'position': 'Lektor',
    #             'role': 'Admin'
    #         },
    #         format='json'
    #     )
    #
    #     self.assertEqual(response.status_code, 200)

    # def test_edit_user_data_user_not_found_returns_HTTP_NOT_FOUND(self):
    #     access = self.login()
    #
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    #     response = self.client.put(
    #         self.manage_users_url,
    #         {
    #             'username': 'doesnotexist',
    #             'email': self.email,
    #             'password': 'test',
    #             'full_name': self.full_name,
    #             'university': 'UGM',
    #             'expertise': 'Art',
    #             'position': 'Lektor',
    #             'role': 'Admin'
    #         },
    #         format='json'
    #     )
    #
    #     self.assertEqual(response.status_code, 404)

    # def test_edit_user_another_user_access_returns_HTTP_UNAUTHORIZED(self):
    #     access = self.login()
    #
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + "akses_user_lain")
    #     response = self.client.put(
    #         self.manage_users_url,
    #         {
    #             'username': self.username,
    #             'email': self.email,
    #             'password': self.password,
    #             'full_name': self.full_name,
    #             'university': 'UGM',
    #             'expertise': 'Art',
    #             'position': 'Lektor',
    #             'role': 'Dosen'
    #         },
    #         format='json'
    #     )
    #
    #     self.assertEqual(response.status_code, 401)

    # def test_edit_user_data_incomplete_request_returns_HTTP_BAD_REQUEST(self):
    # 	access = self.login()

    # 	self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    # 	response = self.client.put(
    # 		self.manage_users_url,
    # 		{
    # 			'username': 'tester',
    # 			'university': 'UGM',
    # 		},
    # 		format='json'
    # 	)

    # 	self.assertEqual(response.status_code, 400)

    # ## == Delete Dosen Tests == ##
    # def test_successful_delete_user_data_returns_HTTP_OK(self):
    #     access = self.login()
    #
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    #     response = self.client.delete(
    #         self.manage_users_url,
    #         {
    #             'username': self.username
    #         }
    #     )
    #
    #     self.assertEqual(response.status_code, 200)

    # def test_delete_user_data_not_found_returns_HTTP_NOT_FOUND(self):
    #     access = self.login()
    #
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    #     response = self.client.delete(
    #         self.manage_users_url,
    #         {
    #             'username': 'doesnotexist'
    #         }
    #     )
    #
    #     self.assertEqual(response.status_code, 404)

    # 	def test_update_data_dosen(self):
    #         response = self.client.post(
    #             self.update_url,
    #             {
    #                 'username': 'testBaru',
    #                 'email': self.email,
    #                 'password': 'testBaru',
    #                 'full_name': self.full_name,
    #                 'university': 'UIBaru',
    #                 'expertise': 'ArtBaru',
    #                 'position': 'Lektor',
    #                 'role': 'Admin'
    #             },
    #             format='json'
    #         )
    #         self.assertEqual(response.status_code, 201)

    # def test_get_all_reviewer(self):
    # 	response = self.client.get(self.get_all_reviewer_url)
    # 	self.assertEqual(response.size, 1)


    # Feature not yet implemented
    def test_download_form_review_xlss(self):
        access = self.login()

        self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
        response = self.client.post(
            "/download-review-form/",
            {
                'review_id':"1"
            },
            content_type='Content-Disposition'
        )
        self.assertEqual(response.status_code, 404)


    # def test_get_user_data_returns_user_data(self):
    #     access = self.login()

    # def test_get_user_data_returns_user_data(self):
    #     access = self.login()
    #
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    #     response = self.client.get(self.manage_users_url)
    #     user_data_json = response.json()[1]
    #
    #     self.assertEqual(user_data_json.get('username'), self.username)
    #     self.assertEqual(user_data_json.get('email'), self.email)

    # def test_get_unauthorized_user_data_returns_HTTP_UNAUTHORIZED(self):
    #     access = self.login()
    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    #     self.loginAsDosen()
    #     response = self.client.get(self.manage_users_url)
    #     self.assertEqual(response.status_code, 401)

    # def test_api_is_user_exist_getter(self):
    #     access = self.login()

    #     self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
    #     response = self.client.get(
    #         self.is_user_exist_url,
    #         {
    #         }
    #     )

    #     self.assertEqual(response.status_code, 200)

    # def test_api_is_user_exist_USER_NOT_FOUND(self):
    #     access = self.login()

    #     response = self.client.post(
    #         self.is_user_exist_url,
    #         {
    #             'username': 'doesnotexist'
    #         }
    #     )

    #     self.assertEqual(response.status_code, 404)

    # def test_api_is_user_exist_USER_OK(self):
    #     access = self.login()

    #     response = self.client.post(
    #         self.is_user_exist_url,
    #         {
    #             'username': self.username
    #         }
    #     )

    #     self.assertEqual(response.status_code, 201)
