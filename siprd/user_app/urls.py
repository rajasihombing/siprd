from django.urls import path, include, re_path
from . import views

app_name = "user_app"

urlpatterns = [
  # path("ping", views.ping, name="ping"),
  path("", views.ViewUserData.as_view()),
  path("get-reviewer-by-karil/<int:karil_id>", views.get_reviewer_by_karil),
  path("is-user-exists", views.IsUserExist.as_view()),
  path("get-linked-users/", views.GetLinkedUsers.as_view()),
  path("manage-user/", views.ManageUsers.as_view()),
  path("get-dosen/", views.get_dosen),
]
