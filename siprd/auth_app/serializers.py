from tabnanny import check
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied
from django.contrib.auth.hashers import check_password

from django.contrib.auth.tokens import PasswordResetTokenGenerator

from main.models import User
from django.conf import settings
from . import google

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
  
    def validate(self, attrs):
        data = super().validate(attrs)
        users = User.objects.filter(username=self.user)
        if users.exists():
            if (users[0].auth_provider == 'google'):
              raise AuthenticationFailed("Please continue your login using google")
            
        return data
class GoogleAuthSerializer(serializers.Serializer):
    auth_token = serializers.CharField(required = False)

    def validate_auth_token(self, auth_token):
        user_data = google.Google.validate(auth_token)
        try:
            user_data['sub']
        except:
            raise serializers.ValidationError(
                'The token is invalid or expired. Please login again.'
            )
        if user_data['aud'] != settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY:

            raise AuthenticationFailed("Client ID doesn't match")

        return user_data


class ResetPasswordRequestSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=25)

    redirect_url = serializers.CharField(max_length=500, required=False)

    class Meta:
        fields = ['username', 'redirect_url']

class SetNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(
        min_length=6, max_length=68, write_only=True)
    token = serializers.CharField(
        min_length=1, write_only=True)
    uidb64 = serializers.CharField(
        min_length=1, write_only=True)

    class Meta:
        fields = ['password', 'token', 'uidb64']

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            token = attrs.get('token')
            uidb64 = attrs.get('uidb64')

            id = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=id)
            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('The reset link is invalid', 401)

            user.set_password(password)
            user.save()

            return user
        except Exception:
            raise AuthenticationFailed('The reset link is invalid', 401)

class ChangePasswordSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True)
    old_password = serializers.CharField(write_only=True)
    new_password = serializers.CharField(min_length=6, write_only=True)
    new_password_confirmed = serializers.CharField(min_length=6, write_only=True)

    class Meta:
        fields = '__all__'

    def validate(self, attrs):
        try:
            username = attrs.get('username')
            old_password = attrs.get('old_password')
            new_password = attrs.get('new_password')
            new_password_confirmed = attrs.get('new_password_confirmed')
            user = User.objects.get(username=username)
            current_password = user.password
            if new_password == new_password_confirmed and check_password(old_password , current_password):
                user.set_password(new_password_confirmed)
                user.save()
                return user
            raise 
        except Exception:
            raise PermissionDenied('Credensial salah', 403)
