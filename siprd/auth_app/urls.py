from django.urls import path, include, re_path
from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView
)

app_name = "auth_app"

urlpatterns = [
    path('request-reset-password/', views.RequestPasswordResetPasswordEmail.as_view(), name="request-reset-password"),
    path('password-reset/<uidb64>/<token>/<username>/', views.PasswordTokenCheckAPI.as_view(), name='password-reset-confirm'),
    path('password-reset-complete/', views.SetNewPasswordAPIView.as_view(), name='password-reset-complete'),
    path('change-password/', views.ChangePasswordAPIView.as_view(), name='change-password'),
    path("ping", views.Ping.as_view(), name="ping"),
    path("ping-with-auth", views.PingAuth.as_view()),
    path("register", views.Register.as_view()),
    path("login/google/", views.LoginGoogle.as_view()),

    # NOTE: Grants users a Refresh-Access token pair.
    # Input: JSON file containing username and password
    # Refresh Token to be stored in local storage
    # Access Token to be stored as a cookie
    # Access Token needed to access views which have the permission_classes variable set as [IsAuthenticated]
    path("token/", views.Login.as_view(), name='token_obtain_pair'),

    # NOTE: Grants the user a new Access token using their existing refresh token
    # If user's Refresh token has expired, they will be logged out
    path("token/refresh/", TokenRefreshView.as_view(), name='token_refresh')
]