from os import access
from django.test import TestCase

import re
from urllib import request, response
from rest_framework.test import APIClient, force_authenticate, APIRequestFactory
from main.models import User
from auth_app.admin import OutstandingTokenAdmin
from .views import SetNewPasswordAPIView
from django.urls import resolve
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import smart_bytes, smart_str, DjangoUnicodeDecodeError


class AuthAppUnitTest(TestCase):
    register_url = "/api/auth/register"
    login_url = "/api/auth/token/"
    login_with_google_url = "/api/auth/login/google/"
    ping_with_auth_url = "/api/auth/ping-with-auth"
    request_reset_password_url = "/api/auth/request-reset-password/"
    reset_password_confirm_url = "/api/auth/password-reset/"
    password_reset_complete_url = "/api/auth/password-reset-complete/"
    change_password_url = "/api/auth/change-password/"
    api_token_url = "/api/auth/token/"
    api_token_refresh_url = "/api/auth/token/refresh/"
    header_prefix = "Bearer "
    email = "test.user@example.com"
    full_name = "Test User"
    username = "tester"
    password = "test"
    username_dosen = "dosen"
    password_dosen = "dosen"
    username_google_user = "googleuser"
    password_google_user = "googleuserpass"
    email_google_user = "google_user@google.com"

    def setUp(self):
        self.client = APIClient(HTTP_HOST='example.com')

        self.tester = User.objects.create_user(
            username = self.username,
            email = self.email,
            password = self.password,
            full_name = self.full_name,
            university = 'UI',
            field_of_study = 'Art',
            position = 'Lektor',
            role = 'Admin'
        )

        self.google_user = User.objects.create_user(
            username = self.username_google_user,
            email = self.email_google_user,
            password = self.password_google_user,
            full_name = 'google user',
            university = 'UI',
            field_of_study = 'Art',
            position = 'Lektor',
            role = 'Admin',
            auth_provider = 'google'
        )

        self.reviewer = User.objects.create_user(
            username = 'reviewer_satu',
            email = 'reviewer_satu@email.com',
            password = 'reviewer_satu',
            full_name = 'reviewer satu',
            university = 'UI',
            field_of_study = 'ML',
            position = 'Lektor Kepala',
            role = 'Reviewer'
        )

        self.dosen = User.objects.create_user(
            username = self.username_dosen,
            email = 'dosen@email.com',
            password = self.password_dosen,
            full_name = 'dosen',
            university = 'UI',
            field_of_study = 'Art',
            position = 'Lektor',
            role = 'Dosen'
        )

    def login(self):
        response = self.client.post(
            self.login_url,
            {
                'username': self.username,
                'password': self.password
            }, format='json')

        _, access = response.json().values()
        return access

    def test_request_reset_password_returns_SUCCESS(self):
        response = self.client.post(
            self.request_reset_password_url,
            {
                'username': self.username
            }
        )
        self.assertEqual(response.json()['success'],"We have sent you a link to reset your password")

    def test_reset_password_complete_returns_SUCCESS(self):
        user = User.objects.get(username=self.username)
        uidb64 = urlsafe_base64_encode(smart_bytes(user.pk))
        token = PasswordResetTokenGenerator().make_token(user)
        response = self.client.patch(
            self.password_reset_complete_url,
            {
                'password':'sussybaka',
                'token' : token,
                'uidb64':uidb64
            }, 
        )
        self.assertEqual(response.json()['success'],True)

    def test_password_reset_confirm_redirected_url(self):
        user = User.objects.get(username=self.username)
        uidb64 = urlsafe_base64_encode(smart_bytes(user.pk))
        token = PasswordResetTokenGenerator().make_token(user)
        password_reset_confirm_url = f'/api/auth/password-reset/{uidb64}/{token}/{self.username}/'
        response = self.client.get(
            password_reset_confirm_url,
        )
        self.assertEqual(response.status_code,302)


    def test_api_token_HTTP_405_Not_Allowed(self):
        response = self.client.get(
            self.api_token_url,
            {
            }
        )

        self.assertEqual(response.status_code, 405)


    def test_api_token_HTTP_200_OK(self):
        response = self.client.post(
            self.api_token_url,
            {
                "username": self.username_dosen,
                "password": self.password_dosen
            }
        )

        self.assertEqual(response.status_code, 200)


    def test_api_token_refresh_HTTP_405_Not_Allowed(self):
        response = self.client.get(
            self.api_token_refresh_url,
            {
            }
        )

        self.assertEqual(response.status_code, 405)


    def test_api_token_refresh_HTTP_200_OK(self):
        response = self.client.post(
            self.api_token_url,
            {
                "username": self.username_dosen,
                "password": self.password_dosen
            }
        )

        refresh = response.json()["refresh"]

        response = self.client.post(
            self.api_token_refresh_url,
            {
                "refresh": refresh
            }
        )

        self.assertEqual(response.status_code, 200)
    
    def test_invalid_uidb64(self):
        
        reset_password_confirm = self.reset_password_confirm_url + "Yasdasd/b3ashi-sadfsdf/a"
        response = self.client.get(
            reset_password_confirm,
            follow=True
        )
        self.assertEqual(response.status_code, 404)

    def test_api_change_password_(self):
        response = self.client.post(
            self.login_url,
            {
                'username': self.dosen.username,
                'password': self.password_dosen
            }, format='json')

        _, access = response.json().values()
        self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)
        response = self.client.post(
            self.change_password_url,
            {
                "username": self.dosen.username,
                "old_password": self.password_dosen,
                "new_password": self.password_dosen + "aaaaaaa",
                "new_password_confirmed": self.password_dosen + "aaaaaaa",
            },format="json"
        )
        self.assertEqual(response.status_code, 200)

    def test_ping_auth(self):
        access = self.login()
        response = self.client.get(
            self.ping_with_auth_url,
            **{'HTTP_AUTHORIZATION': f'Bearer {access}'}  
        )
        self.assertEqual(response.status_code,200)

    def test_ping_can_return_JSON_data(self):
        response = self.client.get('/api/auth/ping')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('message'), 'Ping success!')

    def test_ping_with_auth_can_return_JSON_data(self):
        access = self.login()
        self.client.credentials(HTTP_AUTHORIZATION=self.header_prefix + access)

        response = self.client.get('/api/auth/ping-with-auth')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get('message'), 'You are logged in!')

    def test_api_register_new_user_returns_HTTP_Status_201_CREATED(self):
        response = self.client.post(
            self.register_url,
            {
                'username': 'test',
                'email': 'test.user2@example.com',
                'password': 'test',
                'full_name': self.full_name,
                'university': 'UI',
                'expertise': 'Art',
                'position': 'Lektor',
                'role': 'Admin',
                'auth_token': 'no_token'
            },
            format='json')
        self.assertEqual(response.status_code, 201)

    def test_api_register_new_user_incomplete_returns_HTTP_Status_400_BAD_REQUEST(self):
        response = self.client.post(
            self.register_url,
            {
                'username': 'test',
                'auth_token': 'no_token'
            })
        self.assertEqual(response.status_code, 400)

    def test_login_new_user_returns_HTTP_Status_OK(self):
        self.client.post(
            self.register_url,
            {
                'username': 'test',
                'email': 'test.user2@example.com',
                'password': 'test',
                'full_name': self.full_name,
                'university': 'UI',
                'expertise': 'Art',
                'position': 'Lektor',
                'role': 'Admin',
                'auth_token': 'no_token'
            },
            format='json')

        response = self.client.post(
            self.login_url,
            {
                'username': 'test',
                'password': 'test'
            }, format='json')

        self.assertEqual(response.status_code, 200)

    def test_login_new_user_returns_JWT_token_pair(self):
        self.client.post(
            self.register_url,
            {
                'username': 'test',
                'email': 'test.user2@example.com',
                'password': 'test',
                'full_name': self.full_name,
                'university': 'UI',
                'expertise': 'Art',
                'position': 'Lektor',
                'role': 'Admin',
                'auth_token': 'no_token'
            },
            format='json')

        response = self.client.post(
            self.login_url,
            {
                'username': 'test',
                'password': 'test'
            }, format='json')

        access, refresh = response.json().values()
        self.assertIsNotNone(access)
        self.assertIsNotNone(refresh)
        self.assertEqual(response.status_code, 200)

    def test_login_with_username_for_google_provider_user_returns_HTTP_Status_401_UNAUTHORIZED(self):
        response = self.client.post(
            self.login_url,
            {
                'username': self.username_google_user,
                'password': self.password_google_user
            }, format='json')
        self.assertEqual(response.status_code, 401)
        
    def test_login_with_google_invalid_token_returns_HTTP_Status_400_BAD_REQUEST(self):
        response = self.client.post(
            self.login_with_google_url,
            {
                'auth_token' : 'invalid_token'
            }, format='json')
        self.assertEqual(response.status_code, 400)

    def test_api_register_with_google_invalid_token_returns_HTTP_Status_400_BAD_REQUEST(self):
        response = self.client.post(
            self.register_url,
            {
                'username': 'test',
                'email': 'test.user2@example.com',
                'password': '',
                'full_name': self.full_name,
                'university': 'UI',
                'expertise': 'Art',
                'position': 'Lektor',
                'role': 'Admin',
                'auth_token': 'invalid_token'
            },
            format='json')
        self.assertEqual(response.status_code, 400)

    def test_outstanding_token_admin_has_delete_permission_for_admin(self):
        value = OutstandingTokenAdmin.has_delete_permission(self)
        self.assertTrue(value)