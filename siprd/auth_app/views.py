from main.serializers import UserSerializer
from django.urls import reverse
from django.utils.encoding import smart_bytes, smart_str, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.http import JsonResponse, HttpResponseRedirect
from rest_framework import status, generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.mail import send_mail
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.tokens import RefreshToken
from auth_app.serializers import GoogleAuthSerializer
from django.conf import settings
from main.models import User
from rest_framework_simplejwt.views import TokenObtainPairView

from auth_app.serializers import ResetPasswordRequestSerializer, SetNewPasswordSerializer, ChangePasswordSerializer, MyTokenObtainPairSerializer
from main.models import User
from siprd.settings import EMAIL_HOST_USER
from siprd.settings import FRONTEND_URL

# Create your views here.


class RequestPasswordResetPasswordEmail(generics.GenericAPIView):
    serializer_class = ResetPasswordRequestSerializer

    def post(self, request):
        serializer = ResetPasswordRequestSerializer(data=request.data)
        if serializer.is_valid():
            username = serializer.data['username']
            if User.objects.filter(username=username).exists():
                user = User.objects.get(username=username)
                if user is None:
                    return Response({'failed': "user not found"}, status=404)
                uidb64 = urlsafe_base64_encode(smart_bytes(user.pk))
                token = PasswordResetTokenGenerator().make_token(user)
                email_body = self.send_reset_password_email(
                    current_site=request.META['HTTP_HOST'],
                    redirect_url=request.data.get('redirect_url', ''),
                    user=user,
                    uidb64=uidb64,
                    token=token,
                    username=username
                )
                return Response(
                    {
                        'success': 'We have sent you a link to reset your password',
                        'msg': email_body
                    },
                    status=status.HTTP_200_OK)
            else:
                return Response({'failed': "user not found"}, status=404)

    def send_reset_password_email(self, current_site, redirect_url, user, uidb64, token, username):
        relative_link = reverse(
            'auth_app:password-reset-confirm',
            kwargs={
                'uidb64': uidb64,
                'token': token,
                'username': username
            })

        absurl = 'http://' + current_site + relative_link
        final_reset_link = "{}?redirect_url={}".format(absurl, redirect_url)
        # Create Email Message
        email_subject = "Reset Your Passsword"
        resipient_email = user.email
        email_body = """
        Hello,
        Use link below to reset your password
        {}
        """.format(final_reset_link)

        send_mail(
            subject=email_subject,
            message=email_body,
            from_email=EMAIL_HOST_USER,
            recipient_list=[resipient_email],
            fail_silently=False
        )
        return final_reset_link

    # def costruct_email(username,email, absurl, redirect_url,):


class PasswordTokenCheckAPI(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def get(self, request, uidb64, token, username):
        redirect_url = request.GET.get('redirect_url')
        try:
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                return Response({'error': 'Token is not valid, please request a new one'},
                                status=status.HTTP_400_BAD_REQUEST)
            # TODO change for production
            return HttpResponseRedirect(FRONTEND_URL+"reset-password/" + token + "/" + username + "/" + uidb64)

        except DjangoUnicodeDecodeError as identifier:
            try:
                if not PasswordResetTokenGenerator().check_token(user):
                    # TODO change for production
                    return HttpResponseRedirect(FRONTEND_URL+"token-error/")
            except UnboundLocalError:
                # TODO change for production
                return HttpResponseRedirect(FRONTEND_URL+"token-error/")

# Set new password for forgot password feature (Email Verification)


class SetNewPasswordAPIView(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer

    def patch(self, request):

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'success': True, 'message': 'Password reset success'}, status=status.HTTP_200_OK)


class ChangePasswordAPIView(generics.GenericAPIView):
    serializer_class = ChangePasswordSerializer
    # permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({'success': True, 'message': 'Change Password success'}, status=status.HTTP_200_OK)

# General PING API


class Ping(APIView):
    def get(self, request):
        return JsonResponse({'message': 'Ping success!'})

# Authenticated PING API


class PingAuth(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return JsonResponse({'message': 'You are logged in!'})

# Register API
# Will create a new user in database if valid


class Register(APIView):
    def post(self, request):
        user = request.data
        if (user["auth_token"] != "no_token") :
            serializer = GoogleAuthSerializer(data=user)
            serializer.is_valid(raise_exception=True)
            ((serializer.validated_data)['auth_token'])
            user["password"] = settings.SOCIAL_SECRET_PASSWORD
            user["auth_provider"] = "google"
            del user['auth_token']

        serializer = UserSerializer(data=user)
        if serializer.is_valid():
            account = serializer.save()
            if account:
                return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Login API with Validation


class Login(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer

# Login with Google API
# Will log in the user if valid


class LoginGoogle(APIView):

    serializer_class = GoogleAuthSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_data = ((serializer.validated_data)['auth_token'])

        users = User.objects.filter(
            email=user_data["email"], auth_provider="google")
        if not users.exists():
            raise AuthenticationFailed(
                'User with email ' + user_data["email"] + ' not found.')
        else:
            refresh = RefreshToken.for_user(users[0])
            response = {
                'refresh': str(refresh),
                'access': str(refresh.access_token),
            }
            return Response(response, status=status.HTTP_200_OK)
