# SIPRD
[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/peer-review-kepangkatan-akademik-pepe-el-vande/peer-review-kepangkatan-akademik-siprd/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/peer-review-kepangkatan-akademik-pepe-el-vande/peer-review-kepangkatan-akademik-siprd/commits/staging) [![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/peer-review-kepangkatan-akademik-pepe-el-vande/peer-review-kepangkatan-akademik-siprd/badges/staging/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/peer-review-kepangkatan-akademik-pepe-el-vande/peer-review-kepangkatan-akademik-siprd/commits/staging)

## Current Team (2022)
Product Owner: Raja Aldwyn James Napintor

Scrum Master: Rheznandya Erwanto

Asdos: Muhammad Rifqi

Dev Team:
- Arllivandhya Dani
- Billy Vande Y
- Calmeryan Jireh
- Muhammad Ikhsan Asa Pambayun
- Muhammad Fayaad

## Previous Team (2021)
Product Owner: Rifqi Hilman

Scrum Master: M. Naufal Irbahanna

Devops: Danny August

Dev Team:
- Avatar Putra Pertama Azka
- Jerome Emmanuel
- Johanes Steven
- Rizki Maulana Rahmadi
- Seto Adhi Prasetyo
- Yolanda W. Sirait

## Our Project
Sistem Informasi Peer Review Karya Ilmiah is a website designed to aid in streamlining the peer review process. By providing an online platform for university lecturers, peer-reviewers, and other university staff to submit and review scientific works, our website makes it easy for anyone involved in the peer review process to get the job done.

## Poject Structure
1.  Django (backend)  : [siprd/](./siprd)  
2.  Vue JS (frontend) : [siprd-fe/](./siprd-fe) 


## Requirements
- Python 3.9.5 or later
- pip
- venv
- Docker
- Node.js
- npm

## How To Run This App Locally
### Backend & Database
1. Clone this repository

```bash
git clone git@gitlab.cs.ui.ac.id:ppl-fasilkom-ui/ppl-ki-ganjil-2021-2022/si-peer-review-dosen/siprd.git
```

2. Navigate to the directory `siprd`

3. Create a python virtual environment

```bash
python -m venv env # depending on your computer/os, it may be python3
```

4. Activate the virtual environment

```bash
source env/bin/activate # MacOS / Linux

env\Scripts\activate # Windows
```

5. Install the required dependencies

```bash
pip install -r requirements.txt
```

6. Open up another terminal tab/window in the root siprd folder

7. In the root folder, run docker-compose
```bash
docker-compose up --build
```

8. In the backend directory, run make migrations

```bash
python manage.py makemigrations
python manage.py migrate
```

8.5. (Optional) Run backend tests

```bash
coverage run --source='.' manage.py test
```

9. Models will have been migrated to the postgres image, restart the docker containers to run the dockerized backend.

```bash
CTRL + C # Stop the current docker containers
docker-compose up # Restart the containers
```

### Frontend
1. Navigate to the directory `siprd-fe`

2. Install the dependencies
```bash
npm install
```

3. Run the server
```bash
npm run serve
```
